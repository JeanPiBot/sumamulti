package main

import "fmt"


func main() {
	var suma int

	suma = 0

	for i := 0; i <= 999; i++ {
		if i % 3 == 0 || i % 5 == 0 {
			suma = suma + i
		}
	}

	fmt.Printf("La suma es %v\n", suma)
}