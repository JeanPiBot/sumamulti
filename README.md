# Suma de los Múltiplos de 3 o 5

 Si se listan todos los números naturales menores que 10 que son múltiples de 3 o 5, se obtiene 3,5,6 y 9. La suma de estos múltiplos es 23. Encuentre la suma de todos los múltiplos de 3 o 5 menores que 1000.

# Análisis

Se debe usar un acumulador suma para ir sumando todos los mútiplos de 3 o 5. Se debe usar un ciclo Para que revisará todos los números  menores que 1000, por lo tanto, irá desde 1 hasta 999 con incremento de 1. Por último, se debe agregar una condición para revisar si el número es divisible por 3 o 5

# Run and Building

If you just want to run, you can use a command which is:

```
go run sumaMulti.go
```

Then build it with the go tool:

```
go build sumaMulti.go
```

The command above will build an executable named main in the current directory alongside your source code. Execute it to see extraction of data:

```
./sumaMulti.go
```

__NOTE:__ Just is executable for UNIX system. For execute into Windows system click on the main.exe



# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)